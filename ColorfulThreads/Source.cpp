#include <iostream>
#include <mutex>
#include <thread>
#include <vector>
#include <windows.h>


HANDLE hstdout = GetStdHandle(STD_OUTPUT_HANDLE);

// Remember how things were when we started
CONSOLE_SCREEN_BUFFER_INFO csbi;

std::mutex colorChangeMutex;

void colorPrint(WORD color)
{
	for (int i = 0; i < 100; i++)
	{
		colorChangeMutex.lock();
		SetConsoleTextAttribute(hstdout, color);
		std::cout << "Color" << std::endl;
		SetConsoleTextAttribute(hstdout, csbi.wAttributes);
		colorChangeMutex.unlock();
	}
}

int main()
{
	GetConsoleScreenBufferInfo(hstdout, &csbi);

	const WORD colors[] =
	{
		0x1, 0x2, 0x3, 0x4, 0x5,
		0x6, 0x7, 0x8, 0x9, 0xA,
		0xB, 0xC, 0xD, 0xE, 0xF
	};

	std::vector<std::thread> threads;
	for (auto element : colors)
	{
		threads.push_back(std::thread(colorPrint, element));
	}
	for (auto& th : threads)
	{
		th.join();
	}

	
	return 0;
}