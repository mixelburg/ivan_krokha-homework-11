#include "threads.h"

void I_Love_Threads()
{
	std::cout << "I Love Threads" << std::endl;
}

void call_I_Love_Threads()
{
	std::thread t(I_Love_Threads);
	t.join();
}

void printVector(std::vector<int>& primes)
{
	for (auto val : primes)
	{
		std::cout << val << std::endl;
	}
}

void getPrimes(int low, int high, std::vector<int>& primes)
{
	bool isPrime = true;
	while (low < high)
	{
		isPrime = true;
		if (low == 0 || low == 1)
		{
			isPrime = false;
		}
		else
		{
			for (int i = 2; i <= low / 2; ++i)
			{
				if (low % i == 0)
				{
					isPrime = false;
					break;
				}
			}
		}
		if (isPrime) primes.push_back(low);
		++low;
	}
}

std::vector<int> callGetPrimes(int low, int high)
{
	std::vector<int> primes;

	// get start time
	const auto start = std::chrono::steady_clock::now();

	std::thread t(getPrimes, low, high, std::ref(primes));
	t.join();

	// get end time
	const auto end = std::chrono::steady_clock::now();
	// display time delta
	printTimeDelta(start - end);

	return primes;
}

void writePrimesToFile(int low, int high, Writer writer)
{
	bool isPrime = true;
	while (low < high)
	{
		isPrime = true;
		if (low == 0 || low == 1)
		{
			isPrime = false;
		}
		else
		{
			for (int i = 2; i <= low / 2; ++i)
			{
				if (low % i == 0)
				{
					isPrime = false;
					break;
				}
			}
		}
		if (isPrime)
		{
			std::stringstream ss;
			ss << low;
			std::string dataToWrite;
			ss >> dataToWrite;
			dataToWrite += "\n";
			writer.write(dataToWrite);
		}

		++low;
	}
}

void callWritePrimesMultipleThreads(const int begin, const int end, std::string& filePath, const int numThreads)
{
	const auto synchronizedFile = std::make_shared<SynchronizedFile>(filePath);

	if (numThreads < 1)
	{
		std::cout << "[-] Invalid number of threads: " << numThreads << std::endl;
		return;
	}
	std::cout << "[+] Writing to file with " << numThreads << " threads" << std::endl;

	const int rest = (end - begin) % numThreads;
	const int partition = (end - begin) / numThreads;

	int lowLimit = begin;
	int highLimit = begin + rest;

	std::vector<std::thread> threads;
	for (int i = 1; i <= numThreads; i++)
	{
		highLimit += partition;

		std::cout << "[+] Thread " << i << "/" << numThreads
			<< " with values " << lowLimit << "-" << highLimit << " created" << std::endl;
		threads.push_back(std::thread(writePrimesToFile, lowLimit, highLimit, Writer(synchronizedFile)));

		lowLimit = highLimit;
	}

	int count = 1;
	for (auto& th : threads)
	{
		std::cout << "[+] Thread " << count++ << "/" << numThreads << " joined" << std::endl;
		th.join();
	}

	std::cout << "[+] Writing to file finished, closing the file" << std::endl;
	std::cout << "[+] File closed successfully" << std::endl;
}
