#include "threads.h"

int main(int argc, char** argv)
{
	std::string currPath = argv[0];
	currPath = currPath.substr(0, currPath.find_last_of("\\/"));
	
	const int low = 0;
	const int high = 1000000;
	int numThreads = 8;
	
	std::string outFilePath = currPath + "/primes.txt";
	
	//std::vector<int> primes1;
	//timeIt(getPrimes, low, high, std::ref(primes1));
	//std::vector<int> primes2 = callGetPrimes(low, high);
	//std::cout << std::endl;
	//std::cout << primes1.size() << std::endl;
	//std::cout << primes2.size() << std::endl;

	// get start time
	auto start = std::chrono::steady_clock::now();
	
	callWritePrimesMultipleThreads(low, high, outFilePath, numThreads);
	
	// get end time
	auto end = std::chrono::steady_clock::now();
	// display time delta
	printTimeDelta(start - end);

	///////////////////////////////////////////////////////////////////////////////
	std::cout << std::endl << std::endl << std::endl;
	numThreads = 2;
	// get start time
	start = std::chrono::steady_clock::now();

	callWritePrimesMultipleThreads(low, high, outFilePath, numThreads);

	// get end time
	end = std::chrono::steady_clock::now();
	// display time delta
	printTimeDelta(start - end);
	
	return 0;
}